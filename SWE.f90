program SWE

implicit none

real, dimension(:,:,:),allocatable :: u,i_flux,j_flux,R_grid,R_cell
real, dimension(2) :: Rmid
real :: L,delx,delt
integer :: N,counter,counter_max
integer :: i,j,itime,dfreq
real, dimension(3) :: ul,ur,vec1,vec2,cons
real :: Hf,Vn,g=9.81
character (len=90) :: filename

110 format('data.',i4.4,'.dat')
open(10,file='Inputs')
read(10,*)
read(10,*)L,N,delt,counter_max,dfreq
close(10)

itime = 1

allocate(u(1:N+1,1:N+1,3)) !1 and N+1 are ghost layers
allocate(i_flux(1:N,2:N,3))
allocate(j_flux(2:N,1:N,3))
allocate(R_grid(1:N,1:N,2))
allocate(R_cell(2:N,2:N,2))

!Form a grid
delx = L/(N-1)
do j=1,N
   do i=1,N
      R_grid(i,j,1) = (i-1)*delx
      R_grid(i,j,2) = (j-1)*delx
   end do
end do

do j=2,N
   do i=2,N
      R_cell(i,j,1:2) = 0.25*(R_grid(i,j,1:2)+R_grid(i-1,j,1:2)+R_grid(i-1,j-1,1:2)+R_grid(i,j-1,1:2))
   end do
end do

!Initialize
u(:,:,1) = 1.0
u(:,:,2:3) = 0.0
Rmid = (/L*0.5,L*0.5/)
open(10,file='initial_condition.dat')
do j=2,N 
   do i=2,N
      if(norm2(R_cell(i,j,1:2)-Rmid(1:2)).le.L/8.0) then
        u(i,j,1) = 2.0
      end if
      write(10,'(3e30.15)')R_cell(i,j,1:2),u(i,j,1)
   end do
end do
close(10)


do counter=0,counter_max
  if(mod(counter,dfreq) .eq. 0) then
     write(filename,110)itime
     open(10,file=filename)
     do j=2,N 
       do i=2,N
          write(10,'(3e30.15)')R_cell(i,j,1:2),u(i,j,1)
       end do
     end do
     close(10)
     itime = itime+1
   end if

   !boundary conditions
   !Height
   u(1,:,1) = u(2,:,1) 
   u(:,1,1) = u(:,2,1)
   u(N+1,:,1) = u(N,:,1)
   u(:,N+1,1) = u(:,N,1)
   
   u(1,:,2) = u(2,:,2)
   u(1,:,3) = -u(2,:,3)
   u(N+1,:,2) = u(N,:,2)
   u(N+1,:,3) = -u(N,:,3)

   u(:,1,2) = -u(:,2,2)
   u(:,1,3) = u(:,2,3)
   u(:,N+1,2) = -u(:,N,2)
   u(:,N+1,3) = u(:,N,3)

   !Calculate the fluxes
   !I_FLUXES
   do j=2,N
      do i=1,N
         ul = u(i,j,1:3)
         ur = u(i+1,j,1:3)
         Hf = 0.5*(ul(1)+ur(1))
         Vn = 0.5*(ul(2)+ur(2))
         if(Vn .gt. 0.0) then
           vec1 = (/1.0,ul(2),ul(3)/)
         else 
           vec1 = (/1.0,ur(2),ur(3)/)
         end if
         vec2 = (/0.0,1.0,0.0/)
         i_flux(i,j,1:3) = (Hf*Vn*vec1 + g*Hf**2*0.5*vec2)*delx        
      end do
   end do
   !J-FLUXES
   do j=1,N
      do i=2,N
         ul = u(i,j,1:3)
         ur = u(i,j+1,1:3)
         Hf = 0.5*(ul(1)+ur(1))
         Vn = 0.5*(ul(3)+ur(3))
         if(Vn .gt. 0.0) then
           vec1 = (/1.0,ul(2),ul(3)/)
         else
           vec1 = (/1.0,ur(2),ur(3)/)
         end if
         vec2 = (/0.0,0.0,1.0/)
         j_flux(i,j,1:3) = (Hf*Vn*vec1 + g*Hf**2*0.5*vec2)*delx
      end do
   end do
 
   !Update the solution
   do j=2,N
      do i=2,N
         cons(1) = u(i,j,1)
         cons(2) = u(i,j,1)*u(i,j,2)
         cons(3) = u(i,j,1)*u(i,j,3)
         cons = cons - delt/(delx**2)*(i_flux(i,j,:)-i_flux(i-1,j,:)+j_flux(i,j,:)-j_flux(i,j-1,:))
         u(i,j,1) = cons(1)
         u(i,j,2) = cons(2)/cons(1)
         u(i,j,3) = cons(3)/cons(1)
      end do
   end do
   print*,'counter:',counter,',time:',counter*delt
end do

open(10,file='final.dat')
do j=2,N 
   do i=2,N
      if(norm2(R_cell(i,j,1:2)-Rmid(1:2)).le.L/8.0) then
        u(i,j,1) = 1.0
      end if
      write(10,'(3e30.15)')R_cell(i,j,1:2),u(i,j,1)
   end do
end do
close(10)

end program SWE
