1. Compile the code using:
   "gfortran -fdefault-real-8 SWE.f90"
2. Change inputs if necessary 
   L = length of the square domina
   N = Number of grid points

3. run the code
   "./a.out"

4. Open 'plot.gp' and change the value of the variable 't' to the last number of the 'data.00*.dat' files.  

5. Open gnuplot and type
   load "plot.gp" 
